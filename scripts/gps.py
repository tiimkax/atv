#!/usr/bin/env python

import serial  # pySerial
import rospy
from sensor_msgs.msg import NavSatFix
import time
# import io
import math

def reach_serial():
    # Loeb ATV koordinaate

    # sample rida
    # cc = str("2020/10/07 06:59:05.999       -40.7807       -93.4073        -1.5870   2   8   0.4482   2.8550   1.0128   0.0000   0.0000   0.0000   0.10    0.0")

    cc = str(ser.readline())
    rida = cc.split()
    gpsx = float(rida[2])
    gpsy = float(rida[3])

    return [gpsx, gpsy]

ser = serial.Serial("/dev/ttyACM1", 9600)

print(ser.readline())
print(reach_serial())
def talker():
    pub = rospy.Publisher('gps/fix', NavSatFix, queue_size=10)
    rospy.init_node('gpspub', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        geps = NavSatFix()
        geps.header.frame_id = rospy.get_param('~frame_id', 'gps')
        geps.header.stamp = rospy.Time.now()
        geps.status.status = 1
        geps.status.service = 1
        kordid = reach_serial()
        geps.latitude = kordid[0]
        geps.longitude = kordid[1]
        geps.altitude = 0
        pub.publish(geps)
        rate.sleep()
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
